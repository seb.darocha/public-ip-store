FROM python:latest

RUN mkdir /code
ADD requirements.txt /code/requirements.txt

RUN pip install -r /code/requirements.txt
RUN pip install gunicorn

WORKDIR /code/
RUN django-admin startproject config .
ADD config_docker/ /code/config/
ADD app/ /code/app/

RUN ./manage.py collectstatic

CMD ["gunicorn", "--bind=0.0.0:80", "config.wsgi"]




