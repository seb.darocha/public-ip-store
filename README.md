[![pipeline status](https://gitlab.com/seb.darocha/public-ip-store/badges/master/pipeline.svg)](https://gitlab.com/seb.darocha/public-ip-store/commits/master) [![coverage report](https://gitlab.com/seb.darocha/public-ip-store/badges/master/coverage.svg)](https://gitlab.com/seb.darocha/public-ip-store/commits/master)

# Public IP store

Microservice to handle public IP changes on routers

# Install

Note: Requires python 3.6 and a MongoDB server

```
pip3 install -r requirements.txt
# Create django project in dir "config"
django-admin startproject config .
```

add mongoengine setup to setting.py:
```python
# Add installed apps
INSTALLED_APPS = [
...
    'rest_framework',
    'app'
]

# No need for a database
DATABASES = {
#    'default': {
#        'ENGINE': 'django.db.backends.sqlite3',
#        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#    }
}

# Setup mongodb access
import mongoengine
mongoengine.connect(
    db="public_ip",
    host="localhost"
)

```

add urls to config/urls.py:
```python
urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^api/', include('app.urls')),
]
```

Note : you have to handle static files to see DRF documentation

# Tests

```
pip3 install pytest pytest-django mongomock pytest-coverage
pytest --cov app
```


# Use

list all public IP
```
curl http://localhost:8000/api/ip/
```

Get last IP of machine 42:
```
curl "http://localhost:8000/api/ip/?glpi_id=42&last=1"
```

Ceation of an IP record (if it wasn't already this IP on this machine):
```
curl "http://localhost:8000/api/ip/create_if_ip_change/" --data 'glpi_id=42&date=2018-01-01T00:00:00Z&ip=127.0.0.1'
```

Force creation of an IP:
```
curl "http://localhost:8000/api/ip/" --data 'glpi_id=42&date=2017-01-01T00:00:00Z&ip=127.0.0.1'
```
# Note

In django, to query the IP that have changed:

```python
from app.models import IP
{id: val for id, val in IP.objects.item_frequencies("glpi_id").items() if val > 1}

# get machines that have changed from 2019-04-23 14:42:03
{id: val for id, val in IP.objects.filter(date__gt=datetime(2019,4,23,14,42,3)).item_frequencies("glpi_id").items() if val > 1}
```
