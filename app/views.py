from django.http import JsonResponse
from rest_framework import filters
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_mongoengine import viewsets as meviewsets

from app.models import IP
from app.serializers import IPSerializer

from mongoengine.connection import get_db


MAP_F_ALL_DATE_IP = '''
function() {
    emit(this.glpi_id,
         {'date': this.date, 'ip':this.ip});
}

'''
REDUCE_F_LAST_IP = '''
    function(key, values)
    {
        return values.sort(
            function(a,b){
                return a["date"]-b["date"];
            }
            ).pop()["ip"];
    }
'''


def get_all_last_ip(request):
    list(IP.objects.map_reduce(MAP_F_ALL_DATE_IP,
                               REDUCE_F_LAST_IP,
                               "all_public_ips"))

    db = get_db()

    return JsonResponse([ip for ip in db.all_public_ips.find()], safe=False)


class IPViewSet(meviewsets.ModelViewSet):
    lookup_field = 'id'
    serializer_class = IPSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = ('glpi_id', 'date')
    ordering = ('glpi_id', '-date')

    def get_queryset(self):
        glpi_id = self.request.query_params.get('glpi_id')
        last = self.request.query_params.get('last')

        if glpi_id:
            objects = IP.objects.filter(glpi_id=glpi_id)
        else:
            objects = IP.objects.all()
        if last:
            objects = objects.order_by('-date').limit(1)
        return objects

    @action(detail=False, methods=['post'], name="create if IP change")
    def create_if_ip_change(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        last_ip = IP.objects.filter(
            glpi_id=request.data['glpi_id']).order_by('-date').limit(1).first()

        if not last_ip or last_ip.ip != request.data['ip']:
            self.perform_create(serializer)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data,
                        status=status.HTTP_201_CREATED,
                        headers=headers)
