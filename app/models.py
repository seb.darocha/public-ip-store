# from django.db import models
from mongoengine import Document, fields


class IP(Document):
    glpi_id = fields.IntField(required=True)
    date = fields.DateTimeField(required=True)
    ip = fields.StringField(required=True)
