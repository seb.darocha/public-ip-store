from datetime import datetime
import uuid


import pytest
import mongoengine
from django.utils.timezone import make_aware
from django.utils.dateparse import parse_datetime, parse_date

from app.views import IPViewSet, get_all_last_ip
from app.models import IP

from django.test import Client

@pytest.fixture
def mongodb():
    mongoengine.connection.disconnect()

    dbname = 'testdb-%s' % uuid.uuid1()
    db = mongoengine.connect(dbname, host='mongomock://localhost')
    yield db

    db.drop_database(dbname)
    db.close()


def test_getlastips(mongodb, rf):
    data = {"glpi_id": "13",
            "date": datetime(2019, 1, 1, 0, 0, 0),
            "ip": "127.0.0.1"}
    IP.objects.create(**data)

    data2 = {"glpi_id": "14",
             "date": datetime(2019, 1, 1, 0, 0, 0),
             "ip": "127.0.0.2"}
    IP.objects.create(**data2)

    data3 = {"glpi_id": "13",
             "date": datetime(2019, 1, 2, 0, 0, 0),
             "ip": "127.0.0.3"}
    IP.objects.create(**data3)

    c = Client()
    response = c.get('/api/getlastips/')

    assert response.status_code == 200
    data = response.json()
    assert len(data) == 2

    assert str(data[0]['value']) == str(data3['ip'])

    assert str(data[1]['value']) == str(data2['ip'])


def test_IPViewSet_list(mongodb, rf):
    data = {"glpi_id": "13",
            "date": datetime(2019, 1, 1, 0, 0, 0),
            "ip": "127.0.0.1"}
    ip = IP.objects.create(**data)

    data2 = {"glpi_id": "14",
             "date": datetime(2019, 1, 1, 0, 0, 0),
             "ip": "127.0.0.2"}
    IP.objects.create(**data2)

    data3 = {"glpi_id": "13",
             "date": datetime(2019, 1, 2, 0, 0, 0),
             "ip": "127.0.0.3"}
    ip3 = IP.objects.create(**data3)

    request = rf.get('/api/ip/?glpi_id=13')
    response = IPViewSet.as_view({'get': 'list'})(request)

    assert response.status_code == 200
    assert len(response.data) == 2

    assert str(response.data[0]['glpi_id']) == str(data3['glpi_id'])
    assert str(response.data[0]['ip']) == str(data3['ip'])
    assert parse_datetime(response.data[0]['date']) == make_aware(ip3.date)

    assert str(response.data[1]['glpi_id']) == str(data['glpi_id'])
    assert str(response.data[1]['ip']) == str(data['ip'])
    assert parse_datetime(response.data[1]['date']) == make_aware(ip.date)

    request = rf.get('/api/ip/?glpi_id=13&last=True')
    response = IPViewSet.as_view({'get': 'list'})(request)

    assert response.status_code == 200
    assert len(response.data) == 1
    assert str(response.data[0]['glpi_id']) == str(data3['glpi_id'])
    assert str(response.data[0]['ip']) == str(data3['ip'])
    assert parse_datetime(response.data[0]['date']) == make_aware(ip3.date)


def create_ip(rf, data):
    request = rf.post('/api/ip/create_if_ip_change', data=data, format='json')
    response = IPViewSet.as_view({'post': 'create_if_ip_change'})(request)
    assert response.status_code == 201
    return response


def list_ip(rf, ip=None, last=False):
    url = '/api/ip/'
    if ip:
        url += '?glpi_id=%s' % ip

    request = rf.get(url)
    response = IPViewSet.as_view({'get': 'list'})(request)

    assert response.status_code == 200
    return response


def test_IPViewSet_create(mongodb, rf):
    response = list_ip(rf, 13)
    assert len(response.data) == 0
    data = {"glpi_id": "13",
            "date": "2019-01-01T00:00:00Z",
            "ip": "127.0.0.1"}

    create_ip(rf, data)
    response = list_ip(rf, 13)
    assert len(response.data) == 1

    data_same_ip = data.copy()
    data_same_ip["date"] = "2019-02-01T00:00:00Z"

    create_ip(rf, data_same_ip)
    response = list_ip(rf, 13)

    assert len(response.data) == 1
    assert str(response.data[0]['glpi_id']) == str(data['glpi_id'])
    assert str(response.data[0]['ip']) == str(data['ip'])
    assert parse_date(response.data[0]['date']) == parse_date(data['date'])


    data_same_ip = data.copy()
    data_same_ip["date"] = "2019-03-01T00:00:00Z"
    data_same_ip["ip"] = "127.0.0.2"

    create_ip(rf, data_same_ip)
    response = list_ip(rf, 13)

    assert len(response.data) == 2

    assert str(response.data[1]['glpi_id']) == str(data['glpi_id'])
    assert str(response.data[1]['ip']) == str(data['ip'])
    assert parse_date(response.data[1]['date']) == parse_date(data['date'])

    assert str(response.data[0]['glpi_id']) == str(data_same_ip['glpi_id'])
    assert str(response.data[0]['ip']) == str(data_same_ip['ip'])
    assert parse_date(response.data[0]['date']) == parse_date(data_same_ip['date'])



#def test_IPViewSet_several_create(mongodb, rf):
#    request = rf.get('/api/ip/')
#    response = IPViewSet.as_view({'get': 'list'})(request)
#    assert not response.data
#
#    data = {"glpi_id": "13",
#            "date": datetime(2019, 1, 1, 0, 0, 0),
#            "ip": "127.0.0.1"}
#
#    data2 = {"glpi_id": "14",
#             "date": datetime(2019, 1, 1, 0, 0, 0),
#             "ip": "127.0.0.2"}
#
#    data3 = {"glpi_id": "13",
#             "date": datetime(2019, 1, 2, 0, 0, 0),
#             "ip": "127.0.0.3"}
#
#    request = rf.post('/api/ip/', [data, data2, data3], format='json')
#    response = IPViewSet.as_view({'post': 'list'})(request)
#
#    assert response.status_code == 200
#
#    request = rf.get('/api/ip/')
#    response = IPViewSet.as_view({'get': 'list'})(request)
#
#    assert response.status_code == 200
#    from pprint import pprint
#    pprint([dict(data) for data in response.data])
#    assert len(response.data) == 3
