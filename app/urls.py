from app.views import IPViewSet, get_all_last_ip
from rest_framework_mongoengine import routers as merouters
from django.urls import path

merouter = merouters.DefaultRouter()
merouter.register(r'ip', IPViewSet, basename="ip")

urlpatterns = [
    path('getlastips/', get_all_last_ip)
]

urlpatterns += merouter.urls
