from rest_framework_mongoengine import serializers
from app.models import IP


class IPSerializer(serializers.DocumentSerializer):
    class Meta:
        model = IP
        fields = '__all__'
